alias finder='ofd'
alias backup='/Users/brendan/repos/personal/next-mac/dotfiles/auto-save-bash.sh'

alias reset='clear && source ~/.zshrc'
alias cs='curl cht.sh/$1'
alias path="echo $PATH | tr : '\n'"
alias serve="python -m SimpleHTTPServer 8000"

alias upstream='ggsup'
alias setupstream='ggsup'
alias addaltssh="git remote add altssh $(git remote get-url origin | sed 's/@gitlab.com/@altssh.gitlab.com/')"

alias hn='hncli'
alias hnn='hncli next'
alias hno='hncli open'
alias hnc='hncli comments'
alias hnr='hncli resetr'

export BAT_THEME='TwoDark'

export GITLAB_API_ENDPOINT=https://gitlab.com/api/v4
export GITLAB_API_PRIVATE_TOKEN=$(cat ~/.gitlab-token)

cat ~/gitlab_words

###
# GitLab `lab` CLI section
###

alias ci='lab ci view'
alias traceci='lab ci trace'
alias lint='lab ci lint'

###
# GitLab repo stuff
###
alias testwww='bundle exec rake lint'

###
# Brendan's CLI todo-er
###

#alias todo='cd ~/repos/personal/todo && lab issue list && cd -'

alias t='python ~/repos/opensource/t-gl/t.py --task-dir ~/repos/personal/tasks --list tasks'


export AFTERSHIP_API_KEY=e64c3e67-9f26-449f-8c7f-1a62c28bfa50
